# LIS4381 FALL15 P1
## Cuong Huynh

==============================================

## Requirements

*     Suitably modify meta tags
*     Change title, navigation links, and header tags appropriately
*     Add form controls to match attributes of petstore entity
*     Add the following jQuery validation and regular expressions-- as per the entity attribute requirements (and screenshots below):
	1. *All* input fields, except Notes are required
	2. Use min/max jQuery validation
	3. Use regexp to only allow appropriate characters for each control: pst_name, pst_street, pst_city, pst_state, pst_zip, pst_phone, pst_email, pst_url, pst_ytd_sales, pst_notes Name: provided Street, City: no more than 30 characters must only contain letters, numbers State: must be 2 characters must only contain letters Zip: must be between 5 and 9 characters, inclusive must only contain numbers Phone: must be 10 characters, including area code must only contain numbers Email: provided URL: no more than 100 characters YTD Sales: no more than 10 digits, including decimal point can only contain numbers, and decimal point (if used)
	4. *After* testing jQuery validation, use HTML5 property to limit the number of characters for each control
	5. Research what the following validation code does: valid: 'fa fa-check', invalid: 'fa fa-times', validating: 'fa fa-refresh'
*    Use git to push *all* p1 files and changes

==============================================

## Links
[Bitbucket Repo](https://cnh14e@bitbucket.org/cnh14e/lis4381_fall15_p1)  

[Link to default main page](http://cuongnhuynh.com/repos/p1/index.php)